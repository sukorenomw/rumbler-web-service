<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('postid')->unsigned();
			$table->integer('userid')->unsigned();
			$table->integer('from_user_id')->unsigned();
			$table->foreign('postid')->references('id')->on('posts');
			$table->foreign('userid')->references('id')->on('users');
			$table->foreign('from_user_id')->references('id')->on('users');
			$table->dateTime('seen_at');
			$table->integer('type');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
